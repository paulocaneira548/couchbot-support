.. _platforms:

===============
Other Platforms
===============

.. caution:: Please ensure you have read through the :ref:`Basic Config <basicconfig>` before moving to this section
             This avoids issues such as the bot not announcing anyone.

Whilst the main platforms of Twitch and YouTube have dominated the market, there are other platforms that cater to more specialist
topics and subjects and as we are able, we will add them to our supported platform list.

.. Important:: To remove a creator, run the command again!

-------
Picarto
-------

Catering almost exclusively to artists, Picarto found a market that it could build from and has a loyal viewerbase and streamers that love being there.

.. list-table:: Picarto Commands
   :widths: 25 25 50
   :header-rows: 1

   * - Name
     - Example
     - Usage
   * - picarto
     - ``!cb picarto MattTheDev #discord-channel``
     - Adds a creator to your list to announce when Live.

-----
Steam
-----

The Steam Broadcasting feature has been live since 2015 and used to announce some main events such as the 2018 Dota 2 tournament.
This was a fairly straightforward integration so MattTheDev made it happen!

.. list-table:: Steam Command
   :widths: 25 25 50
   :header-rows: 1

   * - Name
     - Example
     - Usage
   * - steam
     - ``!cb steam MattTheDev #discord-channel``
     - Adds a creator to your list to announce when Live.

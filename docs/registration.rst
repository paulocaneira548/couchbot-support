.. _registration:

============
Registration
============

**CouchBot** has the unique function to allow your members to *register* which in turn means that automated announcements are available to them no matter the platform!
The benefit for everyone is less time manually adding people and automated announcements per platform.
Registered users only need to register once and it applies across all servers.

+-----------------------+-------------------------------+--------------------------------------------------------------------------------------+
| Name                  | Example                       | Usage                                                                                |
+-----------------------+-------------------------------+--------------------------------------------------------------------------------------+
| register              | ``!cb register``              | Registers your streaming account with **CouchBot** to allow automatic announcements. |
+-----------------------+-------------------------------+--------------------------------------------------------------------------------------+
| profile               | ``!cb profile``               | Displayes your streaming profile and all your **CouchBot** linked platforms.         |
+-----------------------+-------------------------------+--------------------------------------------------------------------------------------+
| profile               | ``!cb profile @MattTheDev``   | Shows you another persons registered **CouchBot** profile.                           |
+-----------------------+-------------------------------+--------------------------------------------------------------------------------------+
| defaultplatform       | ``!cb defaultplatform mixer`` | This would set my default streaming platform to mixer personally not server wide.    |
+-----------------------+-------------------------------+--------------------------------------------------------------------------------------+
| defaultplatform clear | ``!cb defaultplatform clear`` | Removes the default platform you set.                                                |
+-----------------------+-------------------------------+--------------------------------------------------------------------------------------+
